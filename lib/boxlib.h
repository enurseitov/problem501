#include <vector>
#include <iostream>
using namespace std;
#ifndef BOXLIB_H
	#define BOXLIB_H
	
	class blackBox{
	private:
	vector<int> addSet;	//keeps the sequence of ADDs parsed from input
	vector<int> getSet;	//keeps the sequence of GETS

	public:
	blackBox(vector<int> addSet, vector<int> getSet);	//constructor
	int solve(ostream &outputStream);					//solve the current box and output results to the outputStream. Return 1 if box is unsolvable (wrong add/set data)
	};
	
	vector<blackBox> parseStream(istream& inStream);
	
	
	
#endif


