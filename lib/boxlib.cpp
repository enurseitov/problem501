#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include "boxlib.h"

using namespace std;

	
blackBox::blackBox(vector<int> addSetArg, vector<int> getSetArg){
	addSet = addSetArg;
	getSet = getSetArg;
};

int blackBox::solve(ostream& outputStream){
	
	multiset<int> boxContents; //sets are sorted by design, multisets (in addition) can contain identical elements
	boxContents.clear();
    multiset<int> ::iterator indexInNumbers;
	int addSetSize = addSet.size();
	int getSetSize = getSet.size();
	int indexInGets = 0;
    
    boxContents.insert(addSet[0]); //fist value goes to the set anyway
    indexInNumbers = boxContents.begin();
   
    
    //some security checks to not accept sequences that are not conforming to problem's definition and can break the algorithm 
    if (getSetSize>addSetSize) return 1;
    if (!is_sorted(begin(getSet),end(getSet))){
		outputStream << "Invalid input data for this box"<<endl;
		return 1; 
		}
    
	    
    for (int numbersAmmount = 1; numbersAmmount < addSetSize; numbersAmmount++) {      

      //check if there are GET commands corresponding to the current box fulfillness
      bool outOfBounds = false;
      while (getSet[indexInGets] == numbersAmmount) {
		//output a value from current indexInGets
        outputStream << *indexInNumbers << endl;
        //move index to the next position
        indexInGets++;
        if (indexInNumbers == --boxContents.end()) {
          outOfBounds = true;
        } else indexInNumbers++;
        
        if (indexInGets>getSetSize-1){
			indexInGets--;
			break;
			} 
		}
      
      //add next element to the numbers set
      boxContents.insert(addSet[numbersAmmount]);

      //if inserted number is to the left, move iterator to the left to keep relative position
      if (addSet[numbersAmmount] < * indexInNumbers && !outOfBounds)     indexInNumbers--;
      else if (addSet[numbersAmmount] > * indexInNumbers && outOfBounds) indexInNumbers++;
      }
    
    outputStream<<endl;
	return 0;
};




vector<blackBox> parseStream(istream& inStream){
	
	vector<blackBox> boxes;
	boxes.clear();
	int numberOfSets;
	inStream >> numberOfSets;	
	
	while (numberOfSets--) {		
		
		int M, N;
		inStream >> M;
		inStream >> N;
		vector<int> inputNumbers;
		vector<int> inputGets;

		int temp;
		for (int i = 0; i < M; i++) {
		  inStream >> temp;
		  inputNumbers.push_back(temp);
		}

		for (int i = 0; i < N; i++) {
		  inStream >> temp;
		  inputGets.push_back(temp);
		}
		
		blackBox box(inputNumbers, inputGets);
		boxes.push_back(box);
		
	}
	
	return boxes;
};
	
	
	




