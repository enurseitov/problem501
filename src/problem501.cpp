#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <algorithm>
#include "HELP.h"
#include "../lib/boxlib.h"
 
using namespace std;


int main (int argc, char *argv[]) { 
	
	int exitCode = 0;
	
	if (argc > 1 && argv[1][0] == '-'){
		if (string(argv[1])=="-h" or string(argv[1])=="-help") cout<<HELP<<endl;
		else cout << "Unknown parameter"<<endl;				
		}
	
		
	else if (argc==1 || argc==3){
		
		istream *inStream;
		ostream *outStream;
		ifstream inFile;
		ofstream outFile;
		
		if (argc==1){ 
			cout << "working with console"<<endl;	
			inStream = &cin;
			outStream = &cout;
			}
		
		else if (argc == 3){
			cout << "working with files"<<endl;				
			inFile.open (argv[1]);
			outFile.open (argv[2]);
			if (inFile.is_open() && outFile.is_open()){
				inStream = &inFile;
				outStream = &outFile;
				}
			else{
				cout << "Error opening file"<<endl;
				return 999;
				}			
		}
		
		//main routine if input is correct
		vector<blackBox> boxes = parseStream(*inStream);	//parse the input stream to the vector of independent boxes	
		
		for(std::vector<blackBox>::iterator box = boxes.begin(); box != boxes.end(); ++box) { //solve every box while outputting to outstream
			blackBox currentBox = *box;
			exitCode+=currentBox.solve(*outStream);
			}
		
		
		if (inFile.is_open())  inFile.close();
		if (outFile.is_open()) outFile.close();
	}
	
	else {
		cout<<HELP;
		
		}	
		
	return exitCode;
}
