#ifndef HELP  
#define HELP "\
Usage:problem501 [OPTIONS] [INFILE OUTFILE] \n\
Option               GNU long option         Meaning\n\
 INFILE, OUTFILE                               Input and output files \n\
 -h                   --help                   Show this help and exit \n\
 \n\
Exit codes: \n\
999 - error opening file(s)\n\
1-999 - number of sequences failed to process\n"
 
#endif
