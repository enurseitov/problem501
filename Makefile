CC=g++
CFLAGS=-c -Wall
LDFLAGS=
SOURCEFOLDER=src/
LIBFOLDER=lib/
SOURCES=$(SOURCEFOLDER)problem501.cpp  $(LIBFOLDER)boxlib.cpp 
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=bin/problem501

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@
clean:
	rm -rf */*.o problem501